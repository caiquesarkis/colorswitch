var canvas;
var ctx;
var startTime = 0, endTime =0 ;
var count = 0;
var dif = 1;
var palete = ['#00E022','#E00035','#E5E9EC','#DDCAD9','#D1B1CB']
var Intervalo = 100
var IntervaloRelativo = 100


function canvasInit(){
    document.body.innerHTML = "<canvas  id='thecanvas'>";
    canvas = document.getElementById("thecanvas")
    ctx = canvas.getContext('2d');
}

function Start(){
    

    Intervalo         =document.getElementById("interval").value
    IntervaloRelativo =document.getElementById("relativeInterval").value

    document.getElementById("MainMenu").remove();
    document.getElementById("help").remove();
    canvasInit()

    MainLoop()
}
  



function MainLoop(){
    var intervalo = Intervalo + Math.random()*IntervaloRelativo
    paint()
    
    
    window.setTimeout(MainLoop,intervalo)
}











function paint(){
     
    if (count == 0){
        dif = 1
    }
    if (count == 1){
        dif = -1
    }

    if (dif > 0){
        _Paint(count)

        
        count+=1
    
    }else{

        _Paint(count)
        count-=1
    }

    console.log(count)
}


function _Paint(count){      
    ctx.fillStyle =palete[count];
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}




